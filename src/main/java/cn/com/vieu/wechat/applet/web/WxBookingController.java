package cn.com.vieu.wechat.applet.web;

import cn.com.vieu.wechat.applet.model.WxaBooking;
import cn.com.vieu.wechat.applet.model.vo.booking.BookingIDVO;
import cn.com.vieu.wechat.applet.model.vo.booking.BookingListFilterVO;
import cn.com.vieu.wechat.applet.service.WxaBookingService;
import cn.com.vieu.wechat.applet.service.impl.WxaBookingServiceImpl;
import cn.com.vieu.wechat.applet.service.impl.WxaMediaServiceImpl;
import cn.com.vieu.wechat.applet.service.wx.impl.WxContextService;
import cn.com.vieu.wechat.applet.util.*;
import com.alibaba.druid.util.StringUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.netty.util.internal.StringUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.*;

/**
 * @author yinghua.li
 * @date 2019-01-15 10:20
 */
@Scope("request")
@RequestMapping(value = "/booking", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RestController
public class WxBookingController extends BaseAction {

    private static Logger logger = LogManager.getLogger(WxBookingController.class);

    private static final String APP_ID = "wx14d4582c89175533";
    private static final String MAC_ID = "1523991471";
    private static final String KEY = "91110105MA01EQJ57C91110105MA01EQ";
    private static final String URL = "https://api.mch.weixin.qq.com/pay/unifiedorder";

    @Autowired
    private WxContextService wxContextService;
    @Autowired
    private WxaBookingService wxaBookingService;
    @Value("${common.filePath.data}")
    private String filePath;

    @Value(("${common.address}"))
    private String address;

    @RequestMapping("/getOpenID")
    public Result getUserOpenID(HttpServletRequest request,HttpServletResponse response,String code){
        Result result = new Result(resultStatus,null,null);
        String resultJson = wxContextService.getUserOpenID(code);
        JSONObject jsonObject = JSON.parseObject(resultJson);
        if(jsonObject.getString("errcode") != null){
            result.setCode(ResultCode.UNAUTHORIZED);
            result.setMessage(jsonObject.getString("errmsg"));
        }else {
            result.setData(jsonObject);
        }

        return result;
    }

    @RequestMapping("/uploadFile")
    public Result create(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Result result = new Result(resultStatus,null,null);

        String path =filePath+"booking/temp/";
        File dir = new File(path);
        if (!dir.exists()) {
            dir.mkdirs();
        }
        request.setCharacterEncoding("UTF-8");

        StandardMultipartHttpServletRequest req = (StandardMultipartHttpServletRequest)request;
        Iterator<String> iterator = req.getFileNames();
        while (iterator.hasNext()) {
            HashMap<String, Object> res = new HashMap<>();
            MultipartFile file = req.getFile(iterator.next());
            // 获取文件名
            String fileNames = file.getOriginalFilename();
            int split = fileNames.lastIndexOf(".");
            //获取上传文件的后缀
            String extName = fileNames.substring(split + 1, fileNames.length());
            //申明UUID
            String uuid = UUID.randomUUID().toString().replace("-", "");

            //组成新的图片名称
            String newName = uuid + "." + extName;
            String destPath = path + newName;
            File file1 = new File(destPath);
            OutputStream out = new FileOutputStream(file1);
            out.write(file.getBytes());
            res.put("url", destPath);
            result.setData(address+"/image/booking/temp/"+newName);
            out.close();
        }
        return result;
    }

    @RequestMapping("/create")
    public Result create(HttpServletRequest request,HttpServletResponse response,
                         @RequestBody WxaBooking wxaBooking){
        Result result = new Result(resultStatus,null,null);

        wxaBooking.setBookingCode(RandomStringUtils.getUniqueCode());
        wxaBooking.setStatus(0);
        wxaBooking.setCreatedOn(DateParser.getCurrentUTCDate());
        wxaBooking.setUpdatedOn(DateParser.getCurrentUTCDate());
        wxaBookingService.insert(wxaBooking);
        //将图片从临时文件夹移入booking_id对应的文件夹
        String startPath = filePath+"booking/temp/"+wxaBooking.getPictures();
        String endPath = filePath +"booking/" + wxaBooking.getBookingId() + File.separator;
        try {
            File startFile = new File(startPath);
            //获取文件夹路径
            File tmpFile = new File(endPath);
            //判断文件夹是否创建，没有创建则创建新文件夹
            if(!tmpFile.exists()){
                tmpFile.mkdirs();
            }
            if (startFile.renameTo(new File(endPath + startFile.getName()))) {
                System.out.println("File is moved successful!");
                result.setMessage("文件移动成功,最终路径"+endPath+startFile.getName());
            } else {
                System.out.println("File is failed to move!");
                result.setMessage("文件移动失败,起始路径:"+startPath);
            }
        }catch (Exception e){
            result.setMessage("文件移动失败,起始路径:"+startPath);
        }
        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("bookingID",wxaBooking.getBookingId());
        resultMap.put("bookingCode",wxaBooking.getBookingCode());
        result.setData(resultMap);
        return result;
    }

    @RequestMapping("/listByUser")
    public Result getBookingList(HttpServletRequest request, HttpServletResponse response,
                                 @RequestBody BookingListFilterVO bookingListFilterVO){
        Result result = new Result(resultStatus,null,null);
        if(validateRequestFieldIsNullOrEmpty(bookingListFilterVO.getOpenID())){
            result.setMessage("用户openID为空");
            result.setCode(ResultCode.MISSING_FIELD);
            return result;
        }

        bookingListFilterVO.setFirstResult(parsePageNumber(bookingListFilterVO.getPage(),bookingListFilterVO.getPageSize()));
        if (bookingListFilterVO.getPageSize() == null || bookingListFilterVO.getPageSize() < 1) {
            bookingListFilterVO.setPageSize(10);
        }

        Map<String,Object> resultMap = new HashMap<>();
        resultMap.put("list",wxaBookingService.listByUser(bookingListFilterVO));
        bookingListFilterVO.setStatus(0);
        resultMap.put("waitForPay",wxaBookingService.listByUser(bookingListFilterVO));
        bookingListFilterVO.setStatus(1);
        resultMap.put("waitForCheck",wxaBookingService.listByUser(bookingListFilterVO));
        result.setData(resultMap);

        return result;
    }

    @RequestMapping("/details")
    public Result details(HttpServletRequest request, HttpServletResponse response,
                         @RequestBody BookingIDVO bookingIDVO){
        Result result = new Result(resultStatus,null,null);
        if(validateRequestFieldIsNullOrEmpty(bookingIDVO.getBookingID())){
            result.setMessage("订单ID为空");
            result.setCode(ResultCode.MISSING_FIELD);
            return result;
        }

        Map<String, Object> map = wxaBookingService.selectByPrimaryKey(bookingIDVO.getBookingID());
        if(map!=null){
            WxaBookingServiceImpl.packageEnumIdToName(map);
            if(map.get("bookingPicture")!=null){
                String fileNames = map.get("bookingPicture").toString();
                map.put("bookingPicture",address+"/image/booking/"+map.get("booking_id")+"/"+fileNames);
            }
            WxaMediaServiceImpl.singleTurn(map,address);
        }
        result.setData(map);

        return result;
    }

    @RequestMapping("/createUnifiedOrder")
    public Result createUnifiedOrder(HttpServletRequest request,HttpServletResponse response) throws Exception {
        Result result = new Result(resultStatus,null,null);
        logger.info("微信 统一下单 接口调用");

        //接受参数(金额)
        String amount = request.getParameter("amount");
        //接受参数(openid)
        String openid = request.getParameter("openID");
        String code = request.getParameter("bookingCode");
        //媒体名称(支付成功显示在微信支付 商品详情中)
        //String name = request.getParameter("mediaName");
        String name = "vieu:"+code;
        //订单号

        String bookingID = request.getParameter("bookingID");
        if(validateRequestFieldIsNullOrEmpty(amount,openid,name,code,bookingID)){
            result.setCode(ResultCode.MISSING_FIELD);
            result.setMessage("booking_id为空");
            return result;
        }

        Integer amountInt = (int)(Float.valueOf(amount) * 100);
        String amountMoney = amountInt.toString();

        //创建hashmap(用户获得签名)
        SortedMap<String, String> paraMap = new TreeMap<>();
        //设置随机字符串
        String nonceStr = UUID.randomUUID().toString().replace("-", "");

        //小程序的appID
        paraMap.put("appid",APP_ID);
        //商户号
        paraMap.put("mch_id",MAC_ID);
        //随机字符串
        paraMap.put("nonce_str", nonceStr);
        //设置请求参数(商品描述)
        paraMap.put("body", name);
        //设置请求参数(商户订单号)
        paraMap.put("out_trade_no", code);
        //设置请求参数(总金额)
        paraMap.put("total_fee", amountMoney);
        //设置请求参数(终端IP)
        paraMap.put("spbill_create_ip", "192.168.1.129");
        //设置请求参数(通知地址)
        paraMap.put("notify_url", "https://vieu.iaddata.com/wxApplet/booking/payCallback");
        //设置请求参数(交易类型)
        paraMap.put("trade_type", "JSAPI");
        //设置请求参数(openid)(在接口文档中 该参数 是否必填项 但是一定要注意 如果交易类型设置成'JSAPI'则必须传入openid)
        paraMap.put("openid", openid);
        //调用逻辑传入参数按照字段名的 ASCII 码从小到大排序（字典序）
        String stringA = HttpUtil.formatUrlMap(paraMap, false, false);
        //第二步，在stringA最后拼接上key得到stringSignTemp字符串，并对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写，得到sign值signValue。(签名)
        String sign = MD5Util.MD5Encode(stringA+"&key="+KEY).toUpperCase();

        //将参数 编写XML格式
        String paramBuffer = "<xml>" +
                "<appid>" + APP_ID + "</appid>" +
                "<mch_id>" + MAC_ID + "</mch_id>" +
                "<nonce_str>" + paraMap.get("nonce_str") + "</nonce_str>" +
                "<sign>" + sign + "</sign>" +
                "<body>" + name + "</body>" +
                "<out_trade_no>" + paraMap.get("out_trade_no") + "</out_trade_no>" +
                "<total_fee>" + paraMap.get("total_fee") + "</total_fee>" +
                "<spbill_create_ip>" + paraMap.get("spbill_create_ip") + "</spbill_create_ip>" +
                "<notify_url>" + paraMap.get("notify_url") + "</notify_url>" +
                "<trade_type>" + paraMap.get("trade_type") + "</trade_type>" +
                "<openid>" + paraMap.get("openid") + "</openid>" +
                "</xml>";
        String remotePortData = getRemotePortData(URL, new String(paramBuffer.getBytes(), "ISO8859-1"));
        Map<String,String> map = doXMLParse(remotePortData);
        Map<String,Object> resultJson = new HashMap<>(2);
        //应该创建 支付表数据
        if("SUCCESS".equals(map.get("result_code"))){
            logger.info("微信回调返回是否支付成功：是");
            //访问DB
            WxaBooking payInfo = wxaBookingService.selectByBookingCode(code);
            if(payInfo.getStatus()==0){
                //修改支付状态
                payInfo.setStatus(1);
                payInfo.setUpdatedOn(DateParser.getCurrentUTCDate());
                //更新Bean
                int sqlRow = wxaBookingService.updateByPrimaryKeySelective(payInfo);
                //判断 是否更新成功
                if(sqlRow == 1){
                    logger.info("微信回调  订单号："+code +",修改状态成功");
                }else{
                    result.setMessage("支付成功，但DB中支付状态未修改成功");
                }
                resultJson.put("prepayId", map.get("prepay_id"));
                resultJson.put("outTradeNo", paraMap.get("out_trade_no"));
                result.setData(resultJson);
            }
        }else{
            result.setMessage(paramBuffer+"......."+remotePortData);
        }
        return result;
    }

    @RequestMapping("/payCallback")
    public Result payCallback(HttpServletRequest request,HttpServletResponse response){
        Result result = new Result(resultStatus,null,null);

        logger.info("微信回调接口方法 start");
        logger.info("微信回调接口 操作逻辑 start");
        String inputLine = "";
        StringBuilder notifyXml = new StringBuilder();
        try {
            while((inputLine = request.getReader().readLine()) != null){
                notifyXml.append(inputLine);
            }
            //关闭流
            request.getReader().close();
            logger.info("微信回调内容信息："+notifyXml);
            //解析成Map
            Map<String,String> map = doXMLParse(notifyXml.toString());
            //判断 支付是否成功
            if("SUCCESS".equals(map.get("result_code"))){
                logger.info("微信回调返回是否支付成功：是");
                //获得 返回的商户订单号
                String outTradeNo = map.get("out_trade_no");
                logger.info("微信回调返回商户订单号："+outTradeNo);
                //访问DB
                WxaBooking payInfo = wxaBookingService.selectByBookingCode(outTradeNo);
                if(payInfo.getStatus()==0){
                    //修改支付状态
                    payInfo.setStatus(1);
                    //更新Bean
                    int sqlRow = wxaBookingService.updateByPrimaryKeySelective(payInfo);
                    //判断 是否更新成功
                    if(sqlRow == 1){
                        logger.info("微信回调  订单号："+outTradeNo +",修改状态成功");
                        //封装 返回值

                        //给微信服务器返回 成功标示 否则会一直询问 咱们服务器 是否回调成功
                        PrintWriter writer = response.getWriter();
                        //返回
                        String buffer = "<xml>" +
                                "<return_code>SUCCESS</return_code>" +
                                "<return_msg>OK</return_msg>" +
                                "</xml>";
                        writer.print(buffer);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return result;
    }
    @RequestMapping("/generateSignature")
    public Result generateSignature(HttpServletRequest request,HttpServletResponse response) throws Exception {
        logger.info("微信 支付接口生成签名 方法开始");
        Result result = new Result(resultStatus,null,null);
        Map<String,Object> resultMap = new HashMap<>();
        //获得参数(微信统一下单接口生成的prepay_id )
        String prepayId = request.getParameter("prepayId");
        //创建 时间戳
        String timeStamp = Long.valueOf(System.currentTimeMillis()).toString();
        //创建 随机串
        String nonceStr = UUID.randomUUID().toString().replace("-", "");
        //创建 MD5
        String signType = "MD5";
        //创建hashmap(用户获得签名)
        SortedMap<String, String> paraMap = new TreeMap<String, String>();
        //设置(小程序ID)(这块一定要是大写)
        paraMap.put("appId", APP_ID);
        //设置(时间戳)
        paraMap.put("timeStamp", timeStamp);
        //设置(随机串)
        paraMap.put("nonceStr", nonceStr);
        //设置(数据包)
        paraMap.put("package", "prepay_id="+prepayId);
        //设置(签名方式)
        paraMap.put("signType", signType);

        //调用逻辑传入参数按照字段名的 ASCII 码从小到大排序（字典序）
        String stringA = HttpUtil.formatUrlMap(paraMap, false, false);
        //第二步，在stringA最后拼接上key得到stringSignTemp字符串，并对stringSignTemp进行MD5运算，再将得到的字符串所有字符转换为大写，得到sign值signValue。(签名)
        String sign = MD5Util.MD5Encode(stringA+"&key="+KEY).toUpperCase();
        if(!StringUtils.isEmpty(sign)){
            //返回签名信息
            resultMap.put("sign", sign);
            //返回随机串(这个随机串是新创建的)
            resultMap.put("nonceStr", nonceStr);
            //返回时间戳
            resultMap.put("timeStamp", timeStamp);
            //返回数据包
            resultMap.put("package", "prepay_id="+prepayId);

            logger.info("微信 支付接口生成签名 设置返回值");
        }
        result.setData(resultMap);
        return result;
    }

    /**
     * 解析xml,返回第一级元素键值对。如果第一级元素有子节点，则此节点的值是子节点的xml数据。
     * @param strxml 1
     * @return 1
     * @throws IOException 1
     */
    @SuppressWarnings("rawtypes")
    private Map<String,String> doXMLParse(String strxml) throws Exception {
        if(null == strxml || "".equals(strxml)) {
            return null;
        }

        logger.info(strxml);
        Map<String,String> m = new HashMap<String,String>();
        InputStream in = String2Inputstream(strxml);
        SAXBuilder builder = new SAXBuilder();
        Document doc = builder.build(in);
        Element root = doc.getRootElement();
        List list = root.getChildren();
        for (Object aList : list) {
            Element e = (Element) aList;
            String k = e.getName();
            String v = "";
            List children = e.getChildren();
            if (children.isEmpty()) {
                v = e.getTextNormalize();
            } else {
                v = getChildrenText(children);
            }

            m.put(k, v);
        }

        //关闭流
        in.close();

        return m;
    }

    private  InputStream String2Inputstream(String str) {
        return new ByteArrayInputStream(str.getBytes());
    }

    /**
     * 获取子结点的xml
     * @param children
     * @return String
     */
    @SuppressWarnings("rawtypes")
    private static String getChildrenText(List children) {
        StringBuilder sb = new StringBuilder();
        if(!children.isEmpty()) {
            for (Object aChildren : children) {
                Element e = (Element) aChildren;
                String name = e.getName();
                String value = e.getTextNormalize();
                List list = e.getChildren();
                sb.append("<").append(name).append(">");
                if (!list.isEmpty()) {
                    sb.append(getChildrenText(list));
                }
                sb.append(value);
                sb.append("</").append(name).append(">");
            }
        }

        return sb.toString();
    }

    /**
     * 方法名: getRemotePortData
     * 描述: 发送远程请求 获得代码示例
     * 参数：  @param urls 访问路径
     * 参数：  @param param 访问参数-字符串拼接格式, 例：port_d=10002&port_g=10007&country_a=
     * 创建人: Xia ZhengWei
     * 创建时间: 2017年3月6日 下午3:20:32
     * 版本号: v1.0
     * 返回类型: String
     */
    private String getRemotePortData(String urls, String param){
        logger.info("港距查询抓取数据----开始抓取外网港距数据");
        try {
            URL url = new URL(urls);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            // 设置连接超时时间
            conn.setConnectTimeout(30000);
            // 设置读取超时时间
            conn.setReadTimeout(30000);
            conn.setRequestMethod("POST");
            if(!StringUtil.isNullOrEmpty(param)) {
                // 主要参数
                conn.setRequestProperty("Origin", "https://sirius.searates.com");
                conn.setRequestProperty("Referer", "https://sirius.searates.com/cn/port?A=ChIJP1j2OhRahjURNsllbOuKc3Y&D=567&G=16959&shipment=1&container=20st&weight=1&product=0&request=&weightcargo=1&");
                // 主要参数
                conn.setRequestProperty("X-Requested-With", "XMLHttpRequest");
            }
            // 需要输出
            conn.setDoInput(true);
            // 需要输入
            conn.setDoOutput(true);
            // 设置是否使用缓存
            conn.setUseCaches(false);
            // 设置请求属性
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            // 维持长连接
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.setRequestProperty("Charset", "UTF-8");

            if(!StringUtil.isNullOrEmpty(param)) {
                // 建立输入流，向指向的URL传入参数
                DataOutputStream dos=new DataOutputStream(conn.getOutputStream());
                dos.writeBytes(param);
                dos.flush();
                dos.close();
            }
            // 输出返回结果
            StringBuilder builder=new StringBuilder();
            InputStream input = conn.getInputStream();
            BufferedReader reader = null;
            try {
                reader= new BufferedReader(new InputStreamReader(input, StandardCharsets.UTF_8));
                String line;
                while((line=reader.readLine())!=null){
                    builder.append(line).append("\n");
                }
            }catch (Exception e){
                e.printStackTrace();
            }finally {
                try {
                    assert reader != null;
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return builder.toString();
        } catch (IOException e) {
            e.printStackTrace();
            logger.info("港距查询抓取数据----抓取外网港距数据发生异常：" + e.getMessage());
        }
        logger.info("港距查询抓取数据----抓取外网港距数据失败, 返回空字符串");
        return "";
    }


    @RequestMapping("/cancelPay")
    public Result cancelPay(HttpServletRequest request,HttpServletResponse response,
                            @RequestBody BookingIDVO bookingIDVO){
        Result result = new Result(resultStatus,null,null);

        if(validateRequestFieldIsNullOrEmpty(bookingIDVO.getBookingID(),bookingIDVO.getStatus())){
            result.setCode(ResultCode.MISSING_FIELD);
            return result;
        }
        WxaBooking wxaBooking = new WxaBooking();
        wxaBooking.setBookingId(bookingIDVO.getBookingID());
        wxaBooking.setStatus(bookingIDVO.getStatus());
        wxaBookingService.updateByPrimaryKeySelective(wxaBooking);

        return result;
    }
}
