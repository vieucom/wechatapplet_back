package cn.com.vieu.wechat.applet.model;

import java.util.Date;
/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public class WxaMedia {
    private Long mediaId;

    private String name;

    private Double mediaFee;

    private String mediaCode;

    private Integer stationId;

    private Double heat;

    private Double level;

    private String details;

    private String flag;

    private String area;

    private Integer craftId;

    private Date lightStart;

    private Date lightEnd;

    private Double actualWidth;

    private Double actualHeight;

    private Double designWidth;

    private Double designHeight;

    private Double visualWidth;

    private Double visualHeight;

    private Integer cityId;

    private Integer routeId;

    private Double fabricationFee;

    private String pictures;

    public Long getMediaId() {
        return mediaId;
    }

    public void setMediaId(Long mediaId) {
        this.mediaId = mediaId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Double getMediaFee() {
        return mediaFee;
    }

    public void setMediaFee(Double mediaFee) {
        this.mediaFee = mediaFee;
    }

    public String getMediaCode() {
        return mediaCode;
    }

    public void setMediaCode(String mediaCode) {
        this.mediaCode = mediaCode == null ? null : mediaCode.trim();
    }

    public Integer getStationId() {
        return stationId;
    }

    public void setStationId(Integer stationId) {
        this.stationId = stationId;
    }

    public Double getHeat() {
        return heat;
    }

    public void setHeat(Double heat) {
        this.heat = heat;
    }

    public Double getLevel() {
        return level;
    }

    public void setLevel(Double level) {
        this.level = level;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details == null ? null : details.trim();
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag == null ? null : flag.trim();
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area == null ? null : area.trim();
    }

    public Integer getCraftId() {
        return craftId;
    }

    public void setCraftId(Integer craftId) {
        this.craftId = craftId;
    }

    public Date getLightStart() {
        return lightStart;
    }

    public void setLightStart(Date lightStart) {
        this.lightStart = lightStart;
    }

    public Date getLightEnd() {
        return lightEnd;
    }

    public void setLightEnd(Date lightEnd) {
        this.lightEnd = lightEnd;
    }

    public Double getActualWidth() {
        return actualWidth;
    }

    public void setActualWidth(Double actualWidth) {
        this.actualWidth = actualWidth;
    }

    public Double getActualHeight() {
        return actualHeight;
    }

    public void setActualHeight(Double actualHeight) {
        this.actualHeight = actualHeight;
    }

    public Double getDesignWidth() {
        return designWidth;
    }

    public void setDesignWidth(Double designWidth) {
        this.designWidth = designWidth;
    }

    public Double getDesignHeight() {
        return designHeight;
    }

    public void setDesignHeight(Double designHeight) {
        this.designHeight = designHeight;
    }

    public Double getVisualWidth() {
        return visualWidth;
    }

    public void setVisualWidth(Double visualWidth) {
        this.visualWidth = visualWidth;
    }

    public Double getVisualHeight() {
        return visualHeight;
    }

    public void setVisualHeight(Double visualHeight) {
        this.visualHeight = visualHeight;
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public Double getFabricationFee() {
        return fabricationFee;
    }

    public void setFabricationFee(Double fabricationFee) {
        this.fabricationFee = fabricationFee;
    }

    public String getPictures() {
        return pictures;
    }

    public void setPictures(String pictures) {
        this.pictures = pictures;
    }
}