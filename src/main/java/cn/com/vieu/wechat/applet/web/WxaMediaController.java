package cn.com.vieu.wechat.applet.web;

import cn.com.vieu.wechat.applet.model.enums.MaterialOfCraftEnum;
import cn.com.vieu.wechat.applet.model.vo.media.MediaIDVO;
import cn.com.vieu.wechat.applet.model.vo.media.MediaListFilterVO;
import cn.com.vieu.wechat.applet.service.WxaCityService;
import cn.com.vieu.wechat.applet.service.WxaMediaService;
import cn.com.vieu.wechat.applet.util.BaseAction;
import cn.com.vieu.wechat.applet.util.EnumUtil;
import cn.com.vieu.wechat.applet.util.Result;
import cn.com.vieu.wechat.applet.util.ResultCode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.sql.Timestamp;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-11
 */
@Scope("request")
@RequestMapping(value = "/media", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
@RestController
public class WxaMediaController extends BaseAction {

    private static Logger logger = LogManager.getLogger(WxaMediaController.class);


    @Autowired
    private WxaMediaService wxaMediaService;
    @Autowired
    private WxaCityService wxaCityService;

    @RequestMapping(value = "/listOfSearchInfo")
    public Result listOfSearchInfo(
            HttpServletRequest request,
            HttpServletResponse response){
        Result result = new Result();

        List<Map<String, Object>> list = wxaCityService.list();

        result.setCode(resultStatus);
        result.setData(list);
        return result;
    }


    @RequestMapping("/list")
    public Result getList(HttpServletRequest request,
                          HttpServletResponse response,
                          @RequestBody(required = false) MediaListFilterVO mediaListFilterVO){
        Result result = new Result();

        if (mediaListFilterVO == null) {
            mediaListFilterVO = new MediaListFilterVO();
        }
        mediaListFilterVO.setFirstResult(parsePageNumber(mediaListFilterVO.getPage(),mediaListFilterVO.getPageSize()));
        if (mediaListFilterVO.getPageSize() == null || mediaListFilterVO.getPageSize() < 1) {
            mediaListFilterVO.setPageSize(10);
        }
        Date[] startEndDate = mediaListFilterVO.getStartEndDate();
        if(startEndDate!=null){
            if(startEndDate.length==2&&startEndDate[0]!=null&&startEndDate[1]!=null){
                mediaListFilterVO.setStartDate(new Timestamp(startEndDate[0].getTime()));
                mediaListFilterVO.setEndDate(new Timestamp(startEndDate[1].getTime()));
            }
        }

        result.setData(wxaMediaService.list(mediaListFilterVO));
        result.setCode(resultStatus);

        return result;
    }

    @RequestMapping("/mediasOfHomePage")
    public Result mediasOfHomePage(HttpServletRequest request,HttpServletResponse response,
                                   @RequestBody MediaListFilterVO mediaListFilterVO){
        Result result = new Result(resultStatus,null,null);
        List<Map<String,Object>> list = wxaMediaService.listHomeMedias(mediaListFilterVO);
        result.setData(list);
        return result;
    }

    @RequestMapping("/details")
    public Result details(HttpServletRequest request,
                          HttpServletResponse response, @RequestBody MediaIDVO mediaIDVO){
        Result result = new Result(resultStatus,null,null);

        if(validateRequestFieldIsNullOrEmpty(mediaIDVO,mediaIDVO.getMediaID())){
            result.setCode(ResultCode.MISSING_FIELD.code);
            result.setMessage(ResultCode.MISSING_FIELD.msg);
            return result;
        }

        Map<String, Object> map = wxaMediaService.selectByPrimaryKey(mediaIDVO.getMediaID());
        if(map!=null){
            if(map.get("craft_id")!=null){
                try {
                    MaterialOfCraftEnum craftEnum = (MaterialOfCraftEnum) EnumUtil.getEnumById(Integer.valueOf(map.get("craft_id").toString()), "id", MaterialOfCraftEnum.class);
                    map.put("craft_name",craftEnum.getName());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        result.setData(map);
        return result;
    }
}
