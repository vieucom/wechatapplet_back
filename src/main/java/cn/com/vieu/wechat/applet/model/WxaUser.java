package cn.com.vieu.wechat.applet.model;
/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public class WxaUser {
    private Integer userId;

    private String openId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId == null ? null : openId.trim();
    }
}