package cn.com.vieu.wechat.applet.service.impl;

import cn.com.vieu.wechat.applet.dao.WxaMediaMapper;
import cn.com.vieu.wechat.applet.model.vo.media.MediaListFilterVO;
import cn.com.vieu.wechat.applet.service.WxaMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-11
 */
@Service("wxaMediaService")
public class WxaMediaServiceImpl implements WxaMediaService {

    @Autowired
    private WxaMediaMapper wxaMediaMapper;

    @Value("${common.filePath.data}")
    private String filePath;

    @Value(("${common.address}"))
    private String address;

    @Override
    public Map<String, Object> list(MediaListFilterVO mediaListFilterVO) {
        Map<String, Object> resultMap = new HashMap<>(2);
        List<Map<String, Object>> list = wxaMediaMapper.list(mediaListFilterVO);
        if (list == null || list.isEmpty()) {
            resultMap.put("list", null);
            resultMap.put("count", 0);
            return resultMap;
        }

        turnToImageUrl(list,address);
        resultMap.put("list", list);
        resultMap.put("count", wxaMediaMapper.countAll(mediaListFilterVO));

        return resultMap;
    }

    @Override
    public Map<String, Object> selectByPrimaryKey(Long mediaId) {
        Map<String, Object> map = wxaMediaMapper.selectByPrimaryKey(mediaId);
        singleTurn(map,address);
        return map;
    }

    public static void singleTurn(Map<String, Object> map,String address) {
        if(map.get("pictures")!=null){
            String[] fileNames = map.get("pictures").toString().split(",");
            String[] pictures =new String[fileNames.length];
            for (int i=0;i<fileNames.length;i++){
                pictures[i] =address+"/image/media/"+map.get("media_id")+"/"+fileNames[i];
            }
            map.put("pictures",pictures);
        }
    }

    @Override
    public List<Map<String, Object>> listHomeMedias(MediaListFilterVO mediaListFilterVO) {
        List<Map<String, Object>> maps = wxaMediaMapper.listHomeMedias(mediaListFilterVO);
        turnToImageUrl(maps,address);
        return maps;
    }

    public static void turnToImageUrl(List<Map<String, Object>> maps,String address) {
        for (Map<String, Object> map:maps) {
            singleTurn(map,address);
        }
    }
}
