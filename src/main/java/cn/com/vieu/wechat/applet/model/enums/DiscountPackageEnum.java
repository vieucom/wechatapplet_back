package cn.com.vieu.wechat.applet.model.enums;

public enum  DiscountPackageEnum {
    /**
     *
     */

    ONE(1,"一块"),

    TWO(2,"两块9折"),

    FIVE(3,"五块8折"),

    TEN(4,"十块7折");

    DiscountPackageEnum(Integer id, String packageName) {
        this.id = id;
        this.packageName = packageName;
    }

    private Integer id;
    private String packageName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
