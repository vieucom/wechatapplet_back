package cn.com.vieu.wechat.applet.model.vo.booking;

/**
 * @author yinghua.li
 * @date 2019-01-16 12:36
 */
public class BookingListFilterVO {

    private Integer page;
    private Integer firstResult;
    private Integer pageSize;
    private String openID;
    private Integer status;

    public String getOpenID() {
        return openID;
    }

    public void setOpenID(String openID) {
        this.openID = openID;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getFirstResult() {
        return firstResult;
    }

    public void setFirstResult(Integer firstResult) {
        this.firstResult = firstResult;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BookingListFilterVO{" +
                "page=" + page +
                ", firstResult=" + firstResult +
                ", pageSize=" + pageSize +
                ", openID='" + openID + '\'' +
                ", status=" + status +
                '}';
    }
}
