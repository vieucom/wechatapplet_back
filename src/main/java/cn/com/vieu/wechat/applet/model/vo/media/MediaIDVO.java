package cn.com.vieu.wechat.applet.model.vo.media;

/**
 * @author yinghua.li
 * @date 2018-11-19 18:18
 */
public class MediaIDVO {

    private Long mediaID;

    public Long getMediaID() {
        return mediaID;
    }

    public void setMediaID(Long mediaID) {
        this.mediaID = mediaID;
    }

    @Override
    public String toString() {
        return "MediaIDVO{" +
                "mediaID=" + mediaID +
                '}';
    }
}
