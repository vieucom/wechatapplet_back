package cn.com.vieu.wechat.applet.model;
/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public class WxaRoute {
    private Integer routeId;

    private String name;

    private Integer cityId;

    private Boolean isActive;

    public Integer getRouteId() {
        return routeId;
    }

    public void setRouteId(Integer routeId) {
        this.routeId = routeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public Integer getCityId() {
        return cityId;
    }

    public void setCityId(Integer cityId) {
        this.cityId = cityId;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }
}