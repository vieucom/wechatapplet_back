package cn.com.vieu.wechat.applet.service;

import cn.com.vieu.wechat.applet.model.vo.media.MediaListFilterVO;

import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-11
 */
public interface WxaMediaService {

    /**
     * get media list
     * @param mediaListFilterVO 1
     * @return 1
     */
    Map<String, Object> list(MediaListFilterVO mediaListFilterVO);

    /**
     * get media details by PrimaryKey
     * @param mediaId 1
     * @return 1
     */
    Map<String, Object> selectByPrimaryKey(Long mediaId);

    /**
     * get medias of home page
     * @param mediaListFilterVO 1
     * @return 1
     */
    List<Map<String, Object>> listHomeMedias(MediaListFilterVO mediaListFilterVO);
}