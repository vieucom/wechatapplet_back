package cn.com.vieu.wechat.applet.service.impl;

import cn.com.vieu.wechat.applet.dao.WxaBookingMapper;
import cn.com.vieu.wechat.applet.model.WxaBooking;
import cn.com.vieu.wechat.applet.model.enums.DiscountPackageEnum;
import cn.com.vieu.wechat.applet.model.enums.PackageEnum;
import cn.com.vieu.wechat.applet.model.vo.booking.BookingListFilterVO;
import cn.com.vieu.wechat.applet.service.WxaBookingService;
import cn.com.vieu.wechat.applet.util.EnumUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-15 14:40
 */
@Service("wxaBookingService")
public class WxaBookingServiceImpl implements WxaBookingService {

    @Autowired
    private WxaBookingMapper wxaBookingMapper;

    @Value(("${common.address}"))
    private String address;

    @Override
    public int insert(WxaBooking record) {
        return wxaBookingMapper.insert(record);
    }

    @Override
    public Map<String, Object> listByUser(BookingListFilterVO bookingListFilterVO) {
        Map<String, Object> resultMap = new HashMap<>(2);
        List<Map<String, Object>> list = wxaBookingMapper.listByUser(bookingListFilterVO);

        if (list == null || list.isEmpty()) {
            resultMap.put("list", null);
            resultMap.put("count", 0);
            return resultMap;
        }
        WxaMediaServiceImpl.turnToImageUrl(list,address);
        for (Map<String, Object> map:list) {
            packageEnumIdToName(map);
        }
        resultMap.put("list", list);
        resultMap.put("count", wxaBookingMapper.countAll(bookingListFilterVO));

        return resultMap;
    }


    public static void packageEnumIdToName(Map<String, Object> map) {
        if(map.get("package")!=null){
            try {
                PackageEnum packageEnum = (PackageEnum) EnumUtil.getEnumById(Integer.valueOf(map.get("package").toString()), "id", PackageEnum.class);
                map.put("package",packageEnum.getPackageName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(map.get("discount_package")!=null){
            try {
                DiscountPackageEnum discountPackageEnum = (DiscountPackageEnum) EnumUtil.getEnumById(Integer.valueOf(map.get("discount_package").toString()), "id", DiscountPackageEnum.class);
                map.put("discount_package",discountPackageEnum.getPackageName());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Map<String, Object> selectByPrimaryKey(Integer bookingId) {
        return wxaBookingMapper.selectByPrimaryKey(bookingId);
    }

    @Override
    public int updateByPrimaryKeySelective(WxaBooking record) {
        return wxaBookingMapper.updateByPrimaryKeySelective(record);
    }

    @Override
    public WxaBooking selectByBookingCode(String bookingCode) {
        return wxaBookingMapper.selectByBookingCode(bookingCode);
    }
}
