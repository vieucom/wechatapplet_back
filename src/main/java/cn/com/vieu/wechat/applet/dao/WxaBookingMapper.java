package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaBooking;
import cn.com.vieu.wechat.applet.model.vo.booking.BookingListFilterVO;

import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-15 14:41
 */
public interface WxaBookingMapper {
    /**
     * insert into booking
     * @param record 1
     * @return 1
     */
    int insert(WxaBooking record);

    /**
     * get bookings by user's openid
     * @param bookingListFilterVO 1
     * @return 1
     */
    List<Map<String, Object>> listByUser(BookingListFilterVO bookingListFilterVO);

    /**
     * get count of bookings
     * @param bookingListFilterVO 1
     * @return 1
     */
    int countAll(BookingListFilterVO bookingListFilterVO);


    /**
     * get booking details by key
     * @param bookingId 1
     * @return 1
     */
    Map<String, Object> selectByPrimaryKey(Integer bookingId);

    /**
     * update by key  selective
     * @param record 1
     * @return 1
     */
    int updateByPrimaryKeySelective(WxaBooking record);

    /**
     * get booking detail by booking code
     * @param bookingCode 1
     * @return 1
     */
    WxaBooking selectByBookingCode(String bookingCode);

    int deleteByPrimaryKey(Integer bookingId);


    int insertSelective(WxaBooking record);


    int updateByPrimaryKey(WxaBooking record);

}