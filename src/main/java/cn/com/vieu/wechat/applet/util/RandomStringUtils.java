package cn.com.vieu.wechat.applet.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author yinghua.li
 * @date 2018-12-07 11:46
 */
public class RandomStringUtils {
    /**
     * 创建修改密码的短信验证码
     * @return 1
     */
    public static String smsCode(){
        return (int)((Math.random()*9+1)*100000)+"";
    }

    /**
     * 获取唯一不重复的编码
     * @return 1
     */
    public static String getUniqueCode(){
        Date date = new Date();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddhhmmssSS");

        return  sdf.format(date).substring(9);
    }
}
