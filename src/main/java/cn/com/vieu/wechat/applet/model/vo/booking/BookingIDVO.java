package cn.com.vieu.wechat.applet.model.vo.booking;

/**
 * @author yinghua.li
 * @date 2019-01-16 15:14
 */
public class BookingIDVO {

    private Integer bookingID;
    private Integer status;

    public Integer getBookingID() {
        return bookingID;
    }

    public void setBookingID(Integer bookingID) {
        this.bookingID = bookingID;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "BookingIDVO{" +
                "bookingID=" + bookingID +
                ", status=" + status +
                '}';
    }
}
