package cn.com.vieu.wechat.applet.dao;

import cn.com.vieu.wechat.applet.model.WxaMedia;
import cn.com.vieu.wechat.applet.model.vo.media.MediaListFilterVO;

import java.util.List;
import java.util.Map;

/**
 * @author yinghua.li
 * @date 2019-01-12 12:49
 */
public interface WxaMediaMapper {

    /**
     * get media list filter by some conditions
     * @param mediaListFilterVO 1
     * @return 1
     */
    List<Map<String,Object>> list(MediaListFilterVO mediaListFilterVO);

    /**
     * total count
     * @param mediaListFilterVO 1
     * @return 1
     */
    long countAll(MediaListFilterVO mediaListFilterVO);

    /**
     * get media details by PrimaryKey
     * @param mediaId 1
     * @return 1
     */
    Map<String, Object> selectByPrimaryKey(Long mediaId);

    /**
     * get medias of home page
     * @param mediaListFilterVO 1
     * @return 1
     */
    List<Map<String, Object>> listHomeMedias(MediaListFilterVO mediaListFilterVO);
    
    int deleteByPrimaryKey(Long mediaId);

    int insert(WxaMedia record);

    int insertSelective(WxaMedia record);


    int updateByPrimaryKeySelective(WxaMedia record);

    int updateByPrimaryKey(WxaMedia record);

}