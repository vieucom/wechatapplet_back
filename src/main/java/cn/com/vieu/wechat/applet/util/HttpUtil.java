package cn.com.vieu.wechat.applet.util;

import com.alibaba.druid.util.StringUtils;
import com.jcabi.http.Request;
import com.jcabi.http.request.JdkRequest;
import com.jcabi.http.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import java.net.HttpURLConnection;
import java.net.URLEncoder;
import java.util.*;

/**
 * http工具类
 *
 * @author hua
 * */
public class HttpUtil {
    private static final Logger logger= LoggerFactory.getLogger(HttpUtil.class);
    public static  String postWithBody(String url,String body){
        try {
            return new JdkRequest(url)
                    .uri()
                    .back()
                    .method(Request.POST)
                    .body().set(body)
                    .back()
                    .header("Content-Type", "application/json")
                    .header("User-Agent", "CRM-SALE")
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }

        return null;
    }


    static  String postWithBodyAndAccessToken(String url, String body, String accessToken){
        try {
            return new JdkRequest(url)
                    .uri().queryParam("access_token",accessToken)
                    .back()
                    .method(Request.POST)
                    .body().set(body)
                    .back()
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }
        return null;
    }

    public static  String postWithParams(String url,Map<String,String> params){
        try {
            return new JdkRequest(url)
                    .uri().queryParams(params)
                    .back()
                    .method(Request.POST)
                    .header("Content-Type", "application/json")
                    .header("User-Agent", "CRM-SALE")
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
        }
        return null;
    }


    public static  String  get(String url, Map<String, String> params){
        try {
            return new JdkRequest(url)
                    .uri().queryParams(params)
                    .back()
                    .method(Request.GET)
                    .fetch().as(RestResponse.class).assertStatus(HttpURLConnection.HTTP_OK).body();
        } catch (Exception e) {
            logger.error("发送HTTP请求时出现异常",e);
            return null;
        }
    }


    /**
     * @Title: getIpAddress
     * @Description: 获取客户端真实IP地址
     * @author yihj
     * @param @param request
     * @param @param response
     * @param @return    参数
     * @return String    返回类型
     * @throws
     */
    public static String getIpAddress(HttpServletRequest request) {
        // 避免反向代理不能获取真实地址, 取X-Forwarded-For中第一个非unknown的有效IP字符串
        String ip = request.getHeader("x-forwarded-for");
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getHeader("WL-Proxy-Client-IP");
        }
        if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)){
            ip = request.getRemoteAddr();
        }
        return ip;
    }

    /**
     *
     * 方法用途: 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序），并且生成url参数串<br>
     * 实现步骤: <br>
     *
     * @param paraMap   要排序的Map对象
     * @param urlEncode   是否需要URLENCODE
     * @param keyToLower    是否需要将Key转换为全小写
     *            true:key转化成小写，false:不转化
     * @return
     */
    public static String formatUrlMap(Map<String, String> paraMap, boolean urlEncode, boolean keyToLower){
        String buff = "";
        Map<String, String> tmpMap = paraMap;
        try
        {
            List<Map.Entry<String, String>> infoIds = new ArrayList<Map.Entry<String, String>>(tmpMap.entrySet());
            // 对所有传入参数按照字段名的 ASCII 码从小到大排序（字典序）
            Collections.sort(infoIds, new Comparator<Map.Entry<String, String>>()
            {
                @Override
                public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2)
                {
                    return (o1.getKey()).toString().compareTo(o2.getKey());
                }
            });
            // 构造URL 键值对的格式
            StringBuilder buf = new StringBuilder();
            for (Map.Entry<String, String> item : infoIds)
            {
                if (!StringUtils.isEmpty(item.getKey()))
                {
                    String key = item.getKey();
                    String val = item.getValue();
                    if (urlEncode)
                    {
                        val = URLEncoder.encode(val, "utf-8");
                    }
                    if (keyToLower)
                    {
                        buf.append(key.toLowerCase() + "=" + val);
                    } else
                    {
                        buf.append(key + "=" + val);
                    }
                    buf.append("&");
                }

            }
            buff = buf.toString();
            if (buff.isEmpty() == false)
            {
                buff = buff.substring(0, buff.length() - 1);
            }
        } catch (Exception e)
        {
            return null;
        }
        return buff;
    }
}
