package cn.com.vieu.wechat.applet.service.wx.impl;

import org.springframework.data.redis.core.*;

import javax.annotation.Resource;

/**
 * spring redis template 父类
 * 基于kv list set  hash zset 在操作元
 * @Auther: WangDong
 * @Date: 2018/4/9 10:40
 */
public abstract class RedisOperationAbstract {

    @Resource()
    protected RedisTemplate<String,Object> redisTemplate;


    protected ZSetOperations<String,Object> zsetOpt(){
        return  redisTemplate.opsForZSet();
    }

    protected ValueOperations<String,Object> kvOpt(){
        return  redisTemplate.opsForValue();
    }

    protected HashOperations<String,String,Object> hashOpt(){
        return  redisTemplate.opsForHash();
    }

    protected ListOperations<String, Object> listOpt(){
        return  redisTemplate.opsForList();
    }

    protected SetOperations<String,Object> setOpt(){
        return  redisTemplate.opsForSet();
    }



}
